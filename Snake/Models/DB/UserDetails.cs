﻿using System;
using System.Collections.Generic;

namespace Snake.Models.DB
{
    public partial class UserDetails
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string HomeAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string HireDate { get; set; }
        public string AreaId { get; set; }
        public string GroupId { get; set; }
        public string Position { get; set; }
        public string Office { get; set; }
        public string HourlyWage { get; set; }
        public string WeeklyWage { get; set; }
        public bool Loi { get; set; }
        public bool Proposals { get; set; }
        public bool M1 { get; set; }
        public bool Contract { get; set; }
        public bool Opportunities { get; set; }
        public bool InboundContract { get; set; }
        public bool Texas { get; set; }
        public bool PendingApproval { get; set; }
        public bool TrueUp { get; set; }
        public bool Cad { get; set; }
        public bool Plancheck { get; set; }
        public bool Ntp { get; set; }
        public bool Permit { get; set; }
        public bool Install { get; set; }
        public bool Substantial { get; set; }
        public bool Interconnection { get; set; }
        public bool PartnerInstall { get; set; }
        public bool WorkComplete { get; set; }
        public bool? ActiveFlag { get; set; }
        public bool Retention { get; set; }
        public bool ProposalOnHold { get; set; }
        public bool ContractOnHold { get; set; }
        public bool PendingCancel { get; set; }
        public bool Cancelled { get; set; }
        public bool Inactive { get; set; }
        public bool OutsideCad { get; set; }
        public bool FundingList { get; set; }
        public bool LeadManagerList { get; set; }
        public bool SiteSurveyList { get; set; }
        public bool MpuList { get; set; }
        public bool EsLeadsList { get; set; }
        public bool UtilityPre { get; set; }
        public bool M1List { get; set; }
        public bool EngineeringList { get; set; }
        public bool CallLog { get; set; }
        public bool LeadGenerator { get; set; }
        public bool DealerRep { get; set; }
        public bool SalesRep { get; set; }
        public bool Proposal { get; set; }
        public bool SalesReport { get; set; }
        public bool CancellationReport { get; set; }
        public bool PayrollReport { get; set; }
        public bool CadOverview { get; set; }
        public bool OpsOverview { get; set; }
        public bool SoldNotInstalled { get; set; }
        public bool Monitoring { get; set; }
        public bool ManageReps { get; set; }
        public bool InterconnectionList { get; set; }
        public bool Reports { get; set; }
        public bool InspectionList { get; set; }
        public bool AllSchedulingList { get; set; }
        public bool AustinSchedulingList { get; set; }
        public bool BakersfieldSchedulingList { get; set; }
        public bool DallasSchedulingList { get; set; }
        public bool DesertSchedulingList { get; set; }
        public bool FresnoSchedulingList { get; set; }
        public bool IowaSchedulingList { get; set; }
        public bool MassachusettsSchedulingList { get; set; }
        public bool RocklinSchedulingList { get; set; }
        public bool SanAntonioSchedulingList { get; set; }
        public bool SanDiegoSchedulingList { get; set; }
        public bool SouthCarolinaSchedulingList { get; set; }
        public bool TemeculaSchedulingList { get; set; }
        public bool UnknownSchedulingList { get; set; }
        public bool UtahSchedulingList { get; set; }
        public bool StageProgressChart { get; set; }
        public bool MonitoringList { get; set; }
        public bool PayrollExport { get; set; }
        public bool InstallDashboard { get; set; }
        public bool ManageUsers { get; set; }
        public bool ManageSettings { get; set; }
        public bool ProposalManager { get; set; }
        public bool CadManager { get; set; }
        public bool Inventory { get; set; }
        public bool Installer { get; set; }
        public bool WhManager { get; set; }
        public string WarehouseId { get; set; }
        public string CrewId { get; set; }
        public bool PurchaseManager { get; set; }
        public bool InstallerLead { get; set; }
        public bool Admin { get; set; }
        public bool Wesco { get; set; }
        public string FmId { get; set; }
        public bool EquipmentSold { get; set; }
        public bool InstallationReport { get; set; }
        public string EmployeeId { get; set; }
        public string Cadidlist { get; set; }
        public string FmCreationAccount { get; set; }
        public string FmCreationStamp { get; set; }
        public string FmLoginName { get; set; }
        public string ProposalWriterIdlist { get; set; }
        public string ProposalWriterIdlist2 { get; set; }
        public string ProposalWriterIdlistLabel { get; set; }
        public string SeenTheNews { get; set; }
        public bool SolarPlus { get; set; }
        public bool SiteSurvey { get; set; }
        public string ConnId { get; set; }
        public string Tsactive { get; set; }
        public bool ShowOnline { get; set; }
        public string Department { get; set; }
        public string AuroraId { get; set; }
        public bool PostInstallImages { get; set; }
        public bool Roofing { get; set; }
        public bool OperationManager { get; set; }
        public bool? ManageCompanyLinks { get; set; }
        public string OmWhList { get; set; }
        public bool InsideSales { get; set; }
        public bool InsideSalesManager { get; set; }
        public bool Advancedsearch { get; set; }
        public bool Legal { get; set; }
        public bool SendToLegalFlag { get; set; }
        public bool? Ahj { get; set; }
        public string BambooId { get; set; }
        public string PaylocityId { get; set; }
        public bool ManageAhj { get; set; }
        public bool ManageInfinityHome { get; set; }
        public bool ManageCrews { get; set; }
        public bool ManageLeadSources { get; set; }
        public bool ManageUtilities { get; set; }
        public bool ManageAdders { get; set; }
        public string Nickname { get; set; }
        public bool ManageHierarchy { get; set; }
        public bool FinanceTeam { get; set; }
        public bool M1permission { get; set; }
        public bool M2permission { get; set; }
        public bool M3permission { get; set; }
        public bool Ptospecialist { get; set; }
        public bool ManageAreas { get; set; }
        public bool Accounting { get; set; }
        public bool OutsideEngineering { get; set; }
        public bool OutsideStructuralEngineering { get; set; }
        public bool OutsideElectricalEngineering { get; set; }
        public int InfyCoin { get; set; }
        public int InfyCoinBudget { get; set; }
        public string RepArea { get; set; }
        public string SunNovaAuthToken { get; set; }
        public string SunNovaRefreshToken { get; set; }
        public string SunNovaId { get; set; }
        public bool Ptomanager { get; set; }
        public bool OutsideEngineeringCa { get; set; }
        public bool OutsideEngineeringTx { get; set; }
        public bool Marketing { get; set; }
        public bool PostInstallCad { get; set; }
        public bool PostInstallTrueUp { get; set; }
        public bool RoofingOperations { get; set; }
        public bool IsSurveyor { get; set; }
        public bool MySqlFlag { get; set; }
        public bool RepFlag { get; set; }
        public string YearlyKws { get; set; }
        public string QuarterlyKws { get; set; }
        public string ProfilePicture { get; set; }
        public string CompanyId { get; set; }
        public string Division { get; set; }
        public string Region { get; set; }
        public string OfficeId { get; set; }
        public string RegionId { get; set; }
        public string DivisionId { get; set; }
        public bool AdminOnlyFlag { get; set; }
        public string DealerName { get; set; }
        public bool DealerLeadFlag { get; set; }
        public bool DealerAdminFlag { get; set; }
        public bool SalesAdminFlag { get; set; }
        public bool AdminLeadFlag { get; set; }
        public bool ManageStoreFlag { get; set; }
        public bool Esflag { get; set; }
        public string PictureRequestStatus { get; set; }
        public bool DeactivatedFlag { get; set; }
        public string LoginCount { get; set; }
        public string OldEsId { get; set; }
        public string LastLoginTimestamp { get; set; }
        public bool? EmailOptIn { get; set; }
        public bool? TextOptIn { get; set; }
        public bool Cadplacard { get; set; }
        public bool ProjectManagement { get; set; }
        public bool ManageFinancings { get; set; }
        public bool ReworkTexas { get; set; }
        public string OneSignalId { get; set; }
        public bool Sgip { get; set; }
        public bool SalesRepOptions { get; set; }
        public bool InboundInstall { get; set; }
        public bool IssueManagement { get; set; }
        public bool M3specialist { get; set; }
        public string BambooSupervisor { get; set; }
        public bool ManageDealers { get; set; }
        public bool? BambooManagerFlag { get; set; }
        public bool FinalInspectionFlag { get; set; }
        public bool WorkOrderReport { get; set; }
        public bool CancellationSpecialist { get; set; }
        public bool IsMputech { get; set; }
        public bool ServiceTech { get; set; }
        public bool IntegratedPartnerInboundInstall { get; set; }
        public bool IntegratedPartnerAdmin { get; set; }
        public bool InstallCompleteBackdate { get; set; }
        public string IntegratedPartnerAreaId { get; set; }
        public string IntegratedPartnerId { get; set; }
        public string IntegratedPartnerUserType { get; set; }
        public string IntegratedPartnerType { get; set; }
        public bool IpsalesAccessFlag { get; set; }
        public bool IpopsAccessFlag { get; set; }
        public bool ServiceAccess { get; set; }
        public bool ServiceTechManager { get; set; }
        public int? NsinternalId { get; set; }
        public bool? IsRoofingSurveyor { get; set; }
        public bool ManageRoofers { get; set; }
        public bool? Dhbspecialist { get; set; }
        public bool CompleteSolar { get; set; }
        public bool PayrollProcessing { get; set; }
        public string ActiveStatus { get; set; }
    }
}
