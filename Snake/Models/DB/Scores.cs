﻿using System;
using System.Collections.Generic;

namespace Snake.Models.DB
{
    public partial class Scores
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public int Score { get; set; }
        public DateTime GameStamp { get; set; }
    }
}
