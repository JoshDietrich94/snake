﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Snake.Models.DB
{
    public partial class TouchstoneContext : DbContext
    {
        public TouchstoneContext()
        {
        }

        public TouchstoneContext(DbContextOptions<TouchstoneContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Scores> Scores { get; set; }
        public virtual DbSet<UserDetails> UserDetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=192.168.0.20;Initial Catalog=Touchstone;User ID=sa;Password=MITTENS1;Max Pool Size=500;Min Pool Size=15;Pooling=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Scores>(entity =>
            {
                entity.Property(e => e.Id)
                    .IsRequired()
                    .HasColumnName("ID")
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GameStamp)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasColumnName("UserID")
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<UserDetails>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ActiveFlag)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ActiveStatus)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Active')");

                entity.Property(e => e.Advancedsearch).HasColumnName("advancedsearch");

                entity.Property(e => e.Ahj)
                    .HasColumnName("ahj")
                    .HasDefaultValueSql("('False')");

                entity.Property(e => e.AllSchedulingList).HasColumnName("All_Scheduling_List");

                entity.Property(e => e.AreaId)
                    .HasColumnName("Area_ID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AuroraId)
                    .HasColumnName("AuroraID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AustinSchedulingList).HasColumnName("Austin_Scheduling_List");

                entity.Property(e => e.BakersfieldSchedulingList).HasColumnName("Bakersfield_Scheduling_List");

                entity.Property(e => e.BambooId)
                    .HasColumnName("BambooID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.BambooSupervisor)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Cad).HasColumnName("CAD");

                entity.Property(e => e.CadManager).HasColumnName("CAD_Manager");

                entity.Property(e => e.CadOverview).HasColumnName("CAD_Overview");

                entity.Property(e => e.Cadidlist)
                    .HasColumnName("CADIDList")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Cadplacard).HasColumnName("CADPlacard");

                entity.Property(e => e.CallLog).HasColumnName("Call_Log");

                entity.Property(e => e.CancellationReport).HasColumnName("Cancellation_Report");

                entity.Property(e => e.City)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyId)
                    .HasColumnName("CompanyID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ConnId)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ContractOnHold).HasColumnName("Contract_On_Hold");

                entity.Property(e => e.CrewId)
                    .HasColumnName("CrewID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DallasSchedulingList).HasColumnName("Dallas_Scheduling_List");

                entity.Property(e => e.DealerName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DealerRep).HasColumnName("Dealer_Rep");

                entity.Property(e => e.Department)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DesertSchedulingList).HasColumnName("Desert_Scheduling_List");

                entity.Property(e => e.Dhbspecialist).HasColumnName("DHBSpecialist");

                entity.Property(e => e.Division)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DivisionId)
                    .HasColumnName("DivisionID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("Email_Address")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.EmailOptIn)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("Employee_ID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.EngineeringList).HasColumnName("Engineering_List");

                entity.Property(e => e.EquipmentSold).HasColumnName("Equipment_Sold");

                entity.Property(e => e.EsLeadsList).HasColumnName("ES_Leads_List");

                entity.Property(e => e.Esflag).HasColumnName("ESFlag");

                entity.Property(e => e.FinanceTeam).HasColumnName("Finance_Team");

                entity.Property(e => e.FirstName)
                    .HasColumnName("First_Name")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FmCreationAccount)
                    .HasColumnName("FM_CreationAccount")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FmCreationStamp)
                    .HasColumnName("FM_CreationStamp")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FmId)
                    .HasColumnName("FM_ID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FmLoginName)
                    .HasColumnName("FM_LoginName")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FresnoSchedulingList).HasColumnName("Fresno_Scheduling_List");

                entity.Property(e => e.FundingList).HasColumnName("Funding_List");

                entity.Property(e => e.GroupId)
                    .HasColumnName("Group_ID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.HireDate)
                    .HasColumnName("Hire_Date")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.HomeAddress)
                    .HasColumnName("Home_Address")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.HourlyWage)
                    .HasColumnName("Hourly_Wage")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.InboundContract).HasColumnName("Inbound_Contract");

                entity.Property(e => e.InsideSales).HasColumnName("Inside_Sales");

                entity.Property(e => e.InsideSalesManager).HasColumnName("Inside_Sales_Manager");

                entity.Property(e => e.InspectionList).HasColumnName("Inspection_List");

                entity.Property(e => e.InstallDashboard).HasColumnName("Install_Dashboard");

                entity.Property(e => e.InstallationReport).HasColumnName("Installation_Report");

                entity.Property(e => e.InstallerLead).HasColumnName("Installer_Lead");

                entity.Property(e => e.IntegratedPartnerAreaId)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IntegratedPartnerId)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IntegratedPartnerType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IntegratedPartnerUserType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InterconnectionList).HasColumnName("Interconnection_List");

                entity.Property(e => e.IowaSchedulingList).HasColumnName("Iowa_Scheduling_List");

                entity.Property(e => e.IpopsAccessFlag).HasColumnName("IPOpsAccessFlag");

                entity.Property(e => e.IpsalesAccessFlag).HasColumnName("IPSalesAccessFlag");

                entity.Property(e => e.IsMputech).HasColumnName("IsMPUTech");

                entity.Property(e => e.IsRoofingSurveyor).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastLoginTimestamp)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasColumnName("Last_Name")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.LeadGenerator).HasColumnName("Lead_Generator");

                entity.Property(e => e.LeadManagerList).HasColumnName("Lead_Manager_List");

                entity.Property(e => e.LoginCount)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasColumnName("Login_Name")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Loi).HasColumnName("LOI");

                entity.Property(e => e.M1List).HasColumnName("M1_List");

                entity.Property(e => e.M1permission).HasColumnName("M1Permission");

                entity.Property(e => e.M2permission).HasColumnName("M2Permission");

                entity.Property(e => e.M3permission).HasColumnName("M3Permission");

                entity.Property(e => e.M3specialist).HasColumnName("M3Specialist");

                entity.Property(e => e.ManageAdders).HasColumnName("Manage_Adders");

                entity.Property(e => e.ManageAhj).HasColumnName("Manage_AHJ");

                entity.Property(e => e.ManageAreas).HasColumnName("Manage_Areas");

                entity.Property(e => e.ManageCompanyLinks)
                    .HasColumnName("Manage_Company_Links")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ManageCrews).HasColumnName("Manage_Crews");

                entity.Property(e => e.ManageDealers).HasColumnName("Manage_Dealers");

                entity.Property(e => e.ManageFinancings).HasColumnName("Manage_Financings");

                entity.Property(e => e.ManageHierarchy).HasColumnName("Manage_Hierarchy");

                entity.Property(e => e.ManageInfinityHome).HasColumnName("Manage_Infinity_Home");

                entity.Property(e => e.ManageLeadSources).HasColumnName("Manage_Lead_Sources");

                entity.Property(e => e.ManageReps).HasColumnName("Manage_Reps");

                entity.Property(e => e.ManageSettings).HasColumnName("Manage_Settings");

                entity.Property(e => e.ManageUsers).HasColumnName("Manage_Users");

                entity.Property(e => e.ManageUtilities).HasColumnName("Manage_Utilities");

                entity.Property(e => e.MassachusettsSchedulingList).HasColumnName("Massachusetts_Scheduling_List");

                entity.Property(e => e.MonitoringList).HasColumnName("Monitoring_List");

                entity.Property(e => e.MpuList).HasColumnName("MPU_List");

                entity.Property(e => e.Nickname)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.NsinternalId).HasColumnName("NSInternalId");

                entity.Property(e => e.Ntp).HasColumnName("NTP");

                entity.Property(e => e.Office)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OfficeId)
                    .HasColumnName("OfficeID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OldEsId)
                    .HasColumnName("OldEsID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OmWhList)
                    .HasColumnName("OM_WH_List")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OneSignalId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OperationManager).HasColumnName("Operation_Manager");

                entity.Property(e => e.OpsOverview).HasColumnName("OPS_Overview");

                entity.Property(e => e.OutsideCad).HasColumnName("Outside_CAD");

                entity.Property(e => e.OutsideEngineeringCa).HasColumnName("OutsideEngineeringCA");

                entity.Property(e => e.OutsideEngineeringTx).HasColumnName("OutsideEngineeringTX");

                entity.Property(e => e.PartnerInstall).HasColumnName("Partner_Install");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.PaylocityId)
                    .HasColumnName("PaylocityID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PayrollExport).HasColumnName("Payroll_Export");

                entity.Property(e => e.PayrollReport).HasColumnName("Payroll_Report");

                entity.Property(e => e.PendingApproval).HasColumnName("Pending_Approval");

                entity.Property(e => e.PendingCancel).HasColumnName("Pending_Cancel");

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("Phone_Number")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PictureRequestStatus)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Position)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PostInstallCad).HasColumnName("PostInstallCAD");

                entity.Property(e => e.ProfilePicture)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalManager).HasColumnName("Proposal_Manager");

                entity.Property(e => e.ProposalOnHold).HasColumnName("Proposal_On_Hold");

                entity.Property(e => e.ProposalWriterIdlist)
                    .HasColumnName("ProposalWriterIDList")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalWriterIdlist2)
                    .HasColumnName("ProposalWriterIDList2")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ProposalWriterIdlistLabel)
                    .HasColumnName("ProposalWriterIDListLabel")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Ptomanager).HasColumnName("PTOManager");

                entity.Property(e => e.Ptospecialist).HasColumnName("PTOSpecialist");

                entity.Property(e => e.PurchaseManager).HasColumnName("Purchase_Manager");

                entity.Property(e => e.QuarterlyKws)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RegionId)
                    .HasColumnName("RegionID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RepArea)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RocklinSchedulingList).HasColumnName("Rocklin_Scheduling_List");

                entity.Property(e => e.SalesRep).HasColumnName("Sales_Rep");

                entity.Property(e => e.SalesReport).HasColumnName("Sales_Report");

                entity.Property(e => e.SanAntonioSchedulingList).HasColumnName("San_Antonio_Scheduling_List");

                entity.Property(e => e.SanDiegoSchedulingList).HasColumnName("San_Diego_Scheduling_List");

                entity.Property(e => e.SeenTheNews)
                    .HasColumnName("seen_the_news")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Sgip).HasColumnName("SGIP");

                entity.Property(e => e.SiteSurveyList).HasColumnName("Site_Survey_List");

                entity.Property(e => e.SoldNotInstalled).HasColumnName("Sold_Not_Installed");

                entity.Property(e => e.SouthCarolinaSchedulingList).HasColumnName("South_Carolina_Scheduling_List");

                entity.Property(e => e.StageProgressChart).HasColumnName("Stage_Progress_Chart");

                entity.Property(e => e.State)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SunNovaAuthToken)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SunNovaId)
                    .HasColumnName("SunNovaID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SunNovaRefreshToken)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.TemeculaSchedulingList).HasColumnName("Temecula_Scheduling_List");

                entity.Property(e => e.TextOptIn)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TrueUp).HasColumnName("True_Up");

                entity.Property(e => e.Tsactive)
                    .HasColumnName("TSActive")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UnknownSchedulingList).HasColumnName("Unknown_Scheduling_List");

                entity.Property(e => e.UtahSchedulingList).HasColumnName("Utah_Scheduling_List");

                entity.Property(e => e.UtilityPre).HasColumnName("Utility_Pre");

                entity.Property(e => e.WarehouseId)
                    .HasColumnName("WarehouseID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.WeeklyWage)
                    .HasColumnName("Weekly_Wage")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.WhManager).HasColumnName("WH_Manager");

                entity.Property(e => e.WorkComplete).HasColumnName("Work_Complete");

                entity.Property(e => e.YearlyKws)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
