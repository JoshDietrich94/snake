﻿using Snake.Models.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake
{
    public partial class Snake : Form
    {
        private readonly List<Circle> Player = new List<Circle>();
        private Circle Food = new Circle();

        public Snake(string UserId, bool Dev = false)
        {
            InitializeComponent();

            new Settings(UserId, Dev);
            gameTimer.Interval = 1000 / Settings.Speed;
            gameTimer.Tick += updateScreen;
            gameTimer.Start();

            startGame();
        }

        private void updateScreen(object sender, EventArgs e)
        {
            if (Settings.GameOver == true && Input.KeyPress(Keys.Enter))
                startGame();
            else if (!Settings.IsPaused)
            {
                Settings.Direction = Settings.Direction switch
                {
                    Directions d when (Input.KeyPress(Keys.Right) && d != Directions.Left) => Directions.Right,
                    Directions d when (Input.KeyPress(Keys.Left) && d != Directions.Right) => Directions.Left,
                    Directions d when (Input.KeyPress(Keys.Up) && d != Directions.Down) => Directions.Up,
                    Directions d when (Input.KeyPress(Keys.Down) && d != Directions.Up) => Directions.Down,
                    _ => Settings.Direction,
                };

                movePlayer();
            }

            pbCanvas.Invalidate();
        }

        private void movePlayer()
        {
            for (int i = Player.Count - 1; i >= 0; i--)
            {
                if (i == 0)
                {
                    switch (Settings.Direction)
                    {
                        case Directions.Right:
                            Player[i].X++;
                            break;
                        case Directions.Left:
                            Player[i].X--;
                            break;
                        case Directions.Up:
                            Player[i].Y--;
                            break;
                        case Directions.Down:
                            Player[i].Y++;
                            break;
                    }

                    if (Player[i].X < 0 || Player[i].Y < 0 ||
                        Player[i].X > Settings.maxXpos || Player[i].Y > Settings.maxYpos)
                        die();

                    for (int j = 1; j < Player.Count; j++)
                        if (Player[i].X == Player[j].X && Player[i].Y == Player[j].Y)
                            die();

                    if (Player[0].X == Food.X && Player[0].Y == Food.Y)
                        eat();
                }
                else
                {
                    Player[i].X = Player[i - 1].X;
                    Player[i].Y = Player[i - 1].Y;
                }
            }
        }

        private void updateGraphics(object sender, PaintEventArgs e)
        {
            Graphics canvas = e.Graphics;

            if (Settings.IsPaused == true)
            {
                string pauseText = "Press New Game in the\nFile menu to begin";
                endText.Text = pauseText;
                endText.Visible = true;
            }
            else if (Settings.GameOver == false)
            {
                Brush snakeColour;
                for (int i = 0; i < Player.Count; i++)
                {
                    if (i == 0)
                        snakeColour = Brushes.Black;
                    else
                        snakeColour = Brushes.Green;

                    canvas.FillEllipse(snakeColour,
                        new Rectangle
                        (
                            Player[i].X * Settings.Width,
                            Player[i].Y * Settings.Height,
                            Settings.Width, Settings.Height
                        ));

                    canvas.FillEllipse(Brushes.Red,
                        new Rectangle
                        (
                            Food.X * Settings.Width,
                            Food.Y * Settings.Height,
                            Settings.Width, Settings.Height
                        ));
                }

                if (Settings.UserIsDev)
                {
                    StringBuilder DebugInfo = new StringBuilder();
                    DebugInfo.AppendLine($"Head Location: ({Player[0].X}, {Player[0].Y})");
                    DebugInfo.AppendLine($"Food Location: ({Food.X}, {Food.Y})");
                    DebugInfo.AppendLine($"Max Positions: ({Settings.maxXpos}, {Settings.maxYpos})");
                    DebugInfo.AppendLine($"Game Speed: {Settings.Speed}");
                    DebugInfo.AppendLine($"Game State: {Settings.IsPaused}");
                    debugInfo.Text = DebugInfo.ToString();
                    debugInfo.Visible = true;
                }
                else
                    debugInfo.Visible = false;

            }
            else
            {
                string gameOver = "";
                if (Settings.Score > Settings.HighScore)
                {
                    Settings.HighScore = Settings.Score;
                    highScoreValue.Text = Settings.HighScore.ToString();
                }
                gameOver += $"Game Over \nFinal Score is {Settings.Score}\nPress enter to Restart\n";
                endText.Text = gameOver;
                endText.Visible = true;
            }
        }

        private void startGame()
        {
            endText.Visible = false;
            var UserId = Settings.UserID;
            var Dev = Settings.UserIsDev;
            var Paused = Settings.IsPaused;
            new Settings(UserId, Dev, Paused);
            Settings.maxXpos = (int)Math.Floor((decimal)((pbCanvas.Size.Width / Settings.Width) - 1));
            Settings.maxYpos = (int)Math.Floor((decimal)((pbCanvas.Size.Height / Settings.Height) - 1));
            Player.Clear();
            Circle head = new Circle { X = Settings.maxXpos / 2, Y = Settings.maxYpos / 2 };
            Player.Add(head);

            scoreValue.Text = Settings.Score.ToString();
            highScoreValue.Text = Settings.HighScore.ToString();

            generateFood();
        }

        private void generateFood()
        {
            Random rnd = new Random();
            Food = new Circle
            {
                X = rnd.Next(0, Settings.maxXpos),
                Y = rnd.Next(0, Settings.maxYpos)
            };
            if (Player.Any(x => x.X == Food.X && x.Y == Food.Y))
                generateFood();
        }

        private void eat()
        {
            Circle body = new Circle
            {
                X = Player[Player.Count - 1].X,
                Y = Player[Player.Count - 1].Y,
            };
            Player.Add(body);

            Settings.Score += Settings.Points;
            scoreValue.Text = Settings.Score.ToString();
            generateFood();
        }

        private void die()
        {
            saveScore();
            Settings.GameOver = true;
        }

        private void saveScore()
        {
            using TouchstoneContext _db = new TouchstoneContext();
            Scores newScore = new Scores
            {
                Id = Guid.NewGuid().ToString(),
                UserId = Settings.UserID,
                Score = Settings.Score,
                GameStamp = DateTime.Now
            };
            _db.Scores.Add(newScore);
            _db.SaveChanges();
        }
    }
}
