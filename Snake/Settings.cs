﻿using Microsoft.EntityFrameworkCore;
using Snake.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snake
{
    public enum Directions
    {
        Left,
        Right,
        Up,
        Down
    };

    class Settings
    {
        public static string UserID { get; set; }
        public static int Width { get; set; }
        public static int Height { get; set; }

        public static int maxXpos { get; set; }
        public static int maxYpos { get; set; }

        public static int Speed { get; set; }
        public static int Score { get; set; }
        public static int HighScore { get; set; }
        public static int Points { get; set; }
        public static bool GameOver { get; set; }
        public static bool IsPaused { get; set; }
        public static bool UserIsDev { get; set; }
        public static Directions Direction { get; set; }

        public Settings(string UserId, bool Dev = false, bool Paused = true)
        {
            UserID = UserId;
            Width = 16;
            Height = 16;
            Speed = 20;
            Score = 0;
            Points = 1;
            GameOver = false;
            IsPaused = Paused;
            Direction = Directions.Down;
            UserIsDev = Dev;

            using TouchstoneContext _db = new TouchstoneContext();
            _db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            var Highest = _db.Scores.OrderByDescending(x => x.Score).FirstOrDefault();
            HighScore = Highest?.Score ?? 0;
        }
    }
}
