﻿using System.Windows.Forms;

namespace Snake
{
    partial class Snake
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pbCanvas = new PictureBox();
            this.debugInfo = new Label();
            this.scoreLabel = new Label();
            this.scoreValue = new Label();
            this.highScoreLabel = new Label();
            this.highScoreValue = new Label();
            this.endText = new Label();
            this.gameTimer = new Timer(this.components);

            this.menuStrip = new MenuStrip();
            this.file = new ToolStripMenuItem();
            this.newGame = new ToolStripMenuItem();
            this.view = new ToolStripMenuItem();
            this.leaderboard = new ToolStripMenuItem();

            ((System.ComponentModel.ISupportInitialize)(this.pbCanvas)).BeginInit();
            this.SuspendLayout();
            // 
            // pbCanvas
            // 
            this.pbCanvas.BackColor = System.Drawing.Color.Gray;
            this.pbCanvas.Location = new System.Drawing.Point(13, 13);
            this.pbCanvas.Name = "pbCanvas";
            this.pbCanvas.Size = new System.Drawing.Size(536, 556);
            this.pbCanvas.TabIndex = 0;
            this.pbCanvas.TabStop = false;
            this.pbCanvas.Paint += new System.Windows.Forms.PaintEventHandler(this.updateGraphics);
            //
            // debugInfo
            //
            this.debugInfo.AutoSize = true;
            this.debugInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.debugInfo.Location = new System.Drawing.Point(0, 571);
            this.debugInfo.Name = "debugInfo";
            this.debugInfo.Size = new System.Drawing.Size(100, 50);
            this.debugInfo.TabIndex = 1;
            this.debugInfo.Text = "";
            this.debugInfo.Visible = false;
            // 
            // scoreLabel
            // 
            this.scoreLabel.AutoSize = true;
            this.scoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scoreLabel.Location = new System.Drawing.Point(610, 61);
            this.scoreLabel.Name = "scoreLabel";
            this.scoreLabel.Size = new System.Drawing.Size(71, 24);
            this.scoreLabel.TabIndex = 1;
            this.scoreLabel.Text = "Score:";
            // 
            // scoreValue
            // 
            this.scoreValue.AutoSize = true;
            this.scoreValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scoreValue.Location = new System.Drawing.Point(687, 61);
            this.scoreValue.Name = "scoreValue";
            this.scoreValue.Size = new System.Drawing.Size(32, 24);
            this.scoreValue.TabIndex = 1;
            this.scoreValue.Text = "0";
            // 
            // highScoreLabel
            // 
            this.highScoreLabel.AutoSize = true;
            this.highScoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.highScoreLabel.Location = new System.Drawing.Point(610, 128);
            this.highScoreLabel.Name = "highScoreLabel";
            this.highScoreLabel.Size = new System.Drawing.Size(121, 24);
            this.highScoreLabel.TabIndex = 3;
            this.highScoreLabel.Text = "High Score:";
            // 
            // highScoreValue
            // 
            this.highScoreValue.AutoSize = true;
            this.highScoreValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.highScoreValue.Location = new System.Drawing.Point(737, 128);
            this.highScoreValue.Name = "highScoreValue";
            this.highScoreValue.Size = new System.Drawing.Size(32, 24);
            this.highScoreValue.TabIndex = 2;
            this.highScoreValue.Text = "00";
            // 
            // endText
            // 
            this.endText.AutoSize = true;
            this.endText.BackColor = System.Drawing.Color.Black;
            this.endText.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.endText.ForeColor = System.Drawing.Color.Yellow;
            this.endText.Location = new System.Drawing.Point(215, 226);
            this.endText.Name = "endText";
            this.endText.Size = new System.Drawing.Size(95, 24);
            this.endText.TabIndex = 1;
            this.endText.Text = "End Text";
            //
            // menuStrip
            //
            this.menuStrip.Items.AddRange(new ToolStripItem[]
            {
                this.file,
                this.view
            });
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(909, 24);
            this.menuStrip.TabIndex = 4;
            this.menuStrip.Text = "menuStrip";
            //
            // file
            //
            this.file.DropDownItems.AddRange(new ToolStripItem[] { this.newGame });
            this.file.Name = "file";
            this.file.Size = new System.Drawing.Size(37, 20);
            this.file.Text = "File";
            //
            // newGame
            //
            this.newGame.Name = "newGame";
            this.newGame.Size = new System.Drawing.Size(180, 22);
            this.newGame.Text = "New Game";
            this.newGame.Click += new System.EventHandler(this.btn_newGame_Click);
            //
            // view
            //
            this.view.DropDownItems.AddRange(new ToolStripItem[] { this.leaderboard });
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(37, 20);
            this.view.Text = "View";
            //
            // leaderboard
            //
            this.leaderboard.Name = "leaderboard";
            this.leaderboard.Size = new System.Drawing.Size(180, 22);
            this.leaderboard.Text = "Leaderboard";
            // 
            // Snake
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ClientSize = new System.Drawing.Size(909, 631);
            this.Controls.Add(this.debugInfo);
            this.Controls.Add(this.endText);
            this.Controls.Add(this.highScoreValue);
            this.Controls.Add(this.highScoreLabel);
            this.Controls.Add(this.scoreValue);
            this.Controls.Add(this.scoreLabel);
            this.Controls.Add(this.pbCanvas);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "Snake";
            this.Text = "Snake";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Snake_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Snake_KeyUp);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Snake_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbCanvas)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private PictureBox pbCanvas;
        private Label debugInfo;
        private Label scoreLabel;
        private Label scoreValue;
        private Label highScoreLabel;
        private Label highScoreValue;
        private Label endText;
        private Timer gameTimer;

        private MenuStrip menuStrip;
        private ToolStripMenuItem file;
        private ToolStripMenuItem newGame;
        private ToolStripMenuItem view;
        private ToolStripMenuItem leaderboard;
    }
}

