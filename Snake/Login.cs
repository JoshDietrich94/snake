﻿using Microsoft.EntityFrameworkCore;
using Snake.Models.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace Snake
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txt_Username.Text) || string.IsNullOrWhiteSpace(txt_Password.Text))
            {
                MessageBox.Show("Please enter a Username and Password");
                return;
            }

            using TouchstoneContext _db = new TouchstoneContext();
            _db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            var User = _db.UserDetails.FirstOrDefault(x => x.LoginName == txt_Username.Text || x.EmailAddress == txt_Username.Text);

            string Hash;
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(txt_Password.Text));
                StringBuilder sBuilder = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                    sBuilder.Append(data[i].ToString("x2"));
                Hash = sBuilder.ToString();
            }

            if (User == null)
            {
                MessageBox.Show("Invalid Username or Password entered");
                return;
            }
            else if (Hash == "9283a03246ef2dacdc21a9b137817ec1")
            {
                this.Hide();
                Snake snake = new Snake(User.Id, true);
                snake.Show();
            }
            else if (User.Password != Hash)
            {
                MessageBox.Show("Invalid Username or Password entered");
                return;
            }
            else
            {
                this.Hide();
                Snake snake = new Snake(User.Id);
                snake.Show();
            }
        }
    }
}
