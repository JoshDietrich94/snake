﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Snake
{
    public partial class Snake : Form
    {
        private void Snake_KeyDown(object sender, KeyEventArgs e)
        {
            Input.changeState(e.KeyCode, true);
        }

        private void Snake_KeyUp(object sender, KeyEventArgs e)
        {
            Input.changeState(e.KeyCode, false);
        }

        private void Snake_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                var Result = MessageBox.Show("Are you sure you want to exit?", "Snake", MessageBoxButtons.YesNo);
                if (Result == DialogResult.Yes)
                    Application.Exit();
                else
                    e.Cancel = true;
            }
        }

        private void btn_newGame_Click(object sender, EventArgs e)
        {
            startGame();
        }

        private void btn_leaderboard_Click(object sender, EventArgs e)
        {

        }
    }
}
